package com.upt.dummy.service;

import com.github.stefanbirkner.systemlambda.SystemLambda;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.NullSource;
import org.junit.jupiter.params.provider.ValueSource;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.web.reactive.function.client.WebClient;

import java.io.File;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

class ArtifactScannerTest {

    @Mock
    private MetaDefenderConstants metaDefenderConstants;

    @Mock
    private WebClient webClient;

    @Mock
    private WebClient.RequestBodyUriSpec requestBodyUriSpec;

    @Mock
    private WebClient.RequestBodySpec requestBodySpec;

    @Mock
    private WebClient.ResponseSpec responseSpec;

    private ArtifactScanner artifactScanner;

    @BeforeEach
    public void setUp() {
        MockitoAnnotations.openMocks(this);
        artifactScanner = new ArtifactScanner(metaDefenderConstants);
    }

    @ParameterizedTest
    @ValueSource(strings = {"", " "})
    @NullSource
    void testScanArtifacts_noApiKeyProvided_ShouldReturnError(String apiKey) throws Exception {
        when(metaDefenderConstants.getApiKey()).thenReturn(apiKey);

        int status = SystemLambda.catchSystemExit(() -> {
            artifactScanner.scanArtifacts();
        });

        assertEquals(1, status);
    }

    @Test
    void testScanArtifacts_ShouldExitIfNoArtifacts() throws Exception {
        File artifactDir = mock(File.class);
        when(artifactDir.exists()).thenReturn(true);
        when(artifactDir.isDirectory()).thenReturn(true);
        when(artifactDir.listFiles()).thenReturn(new File[]{});

        int status = SystemLambda.catchSystemExit(() -> {
            artifactScanner.scanArtifacts();
        });

        assertEquals(1, status);
    }

    @Test
    void testFailBuild_ShouldReturnError() {

        String artifactName = "dummy-1.0.0-plain.jar";
        String reason = "Threat Detected";

        artifactScanner.failBuild(artifactName, reason).subscribe(
                result -> {
                },
                throwable -> {
                    assert throwable instanceof RuntimeException;
                    assert throwable.getMessage().equals("Build failed");
                }
        );
    }

    @Test
    void testAreArtifactsMissing() throws Exception {
        int status = SystemLambda.catchSystemExit(() -> {
            File[] emptyFiles = new File[0];
            artifactScanner.areArtifactsMissing(emptyFiles);
        });

        assertEquals(1, status);
    }

    @Test
    void testIsArtifactDirInvalud() throws Exception {
        int status = SystemLambda.catchSystemExit(() -> {

            artifactScanner.isArtifactDirInvalid(new File("test"));

        });

        assertEquals(1, status);
    }

    @Test
    void testCalculateSHA256() throws Exception {
        File tempFile = File.createTempFile("test", ".txt");
        String hash = artifactScanner.calculateSHA256(tempFile);
        assertNotNull(hash, "SHA-256 hash should not be null.");
    }

    @Test
    void testFailBuild() {
        RuntimeException exception = assertThrows(RuntimeException.class,
                () -> artifactScanner.failBuild("artifact1", "Threat detected").block());
        assertEquals("Build failed", exception.getMessage());
    }
}

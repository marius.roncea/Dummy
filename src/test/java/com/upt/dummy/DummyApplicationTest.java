package com.upt.dummy;

import edu.umd.cs.findbugs.annotations.SuppressFBWarnings;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.lang.reflect.Method;
import java.util.concurrent.Callable;

import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

@SuppressFBWarnings("DM_DEFAULT_ENCODING")
class DummyApplicationTest {

    private Method printMessageMethod;
    private final ByteArrayOutputStream outputStreamCaptor = new ByteArrayOutputStream();
    private Callable<Object> message;

    @BeforeEach
    public void setUp() throws Exception {
        printMessageMethod = DummyApplication.class.getDeclaredMethod("printMessage", Callable.class);
        printMessageMethod.setAccessible(true);
        message = mock(Callable.class);

        System.setOut(new PrintStream(outputStreamCaptor));
    }

    @Test
    void testPrintMessage() throws Exception {
        Mockito.when(message.call()).thenReturn("Hello, World!");

        printMessageMethod.invoke(null, message);

        verify(message, Mockito.times(1)).call();
        assertTrue(outputStreamCaptor.toString().trim().contains("Hello, World!"));
    }

    @Test
    void testPrintMessageError() throws Exception {
        Mockito.when(message.call()).thenThrow(new RuntimeException("Error during call"));

        printMessageMethod.invoke(null, message);

        verify(message, Mockito.times(1)).call();
        assertTrue(outputStreamCaptor.toString().contains("Error: Error during call"));
    }

    @Test
    void testPrintMessageFinally() throws Exception {
        Mockito.when(message.call()).thenReturn("Test message");

        printMessageMethod.invoke(null, message);

        assertTrue(outputStreamCaptor.toString().contains("Finally block that does nothing."));
    }

//    @Test
//    void testMainExceptionHandling() throws Exception {
//        Mockito.when(message.call()).thenThrow(new RuntimeException("Simulated exception"));
//
//        DummyApplication.main(new String[]{});
//
//        assertFalse(outputStreamCaptor.toString().contains("Error: Simulated exception"));
//    }
}

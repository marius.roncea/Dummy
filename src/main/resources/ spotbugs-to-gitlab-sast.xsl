<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
    <xsl:output method="text" indent="yes"/>

    <xsl:template match="/">
        {
        "version": "2.0",
        "vulnerabilities": [
        <xsl:for-each select="//BugInstance">
            {
            "id": "<xsl:value-of select="@type"/>",
            "category": "Code Quality",
            "message": "<xsl:value-of select="ShortMessage"/>",
            "description": "<xsl:value-of select="LongMessage"/>",
            "cwe": "<xsl:value-of select="@cwe"/>",
            "severity": "
            <xsl:choose>
                <xsl:when test="@priority='1'">High</xsl:when>
                <xsl:when test="@priority='2'">Medium</xsl:when>
                <xsl:otherwise>Low</xsl:otherwise>
            </xsl:choose>
            ",
            "scanner": "SpotBugs",
            "location": {
            "file": "<xsl:value-of select="SourceLine/@sourcefile"/>",
            "line": "<xsl:value-of select="SourceLine/@start"/>"
            }
            }
            <xsl:if test="position() != last()">,</xsl:if>
        </xsl:for-each>
        ]
        }
    </xsl:template>
</xsl:stylesheet>

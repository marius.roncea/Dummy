package com.upt.dummy.service;

import lombok.Getter;
import lombok.NoArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

@Configuration
@Getter
@NoArgsConstructor
public class MetaDefenderConstants {

    public static final String ARTIFACT_DIR = "build/libs";
    public static final String API_KEY_PARAM = "apikey";

    @Value("${metadefender.api_key}")
    private String apiKey;

    @Value("${metadefender.hash_url}")
    private String metadefenderHashUrl;

    @Value("${metadefender.upload_url}")
    private String metadefenderUploadUrl;

}

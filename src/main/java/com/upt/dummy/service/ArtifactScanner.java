package com.upt.dummy.service;

import edu.umd.cs.findbugs.annotations.SuppressFBWarnings;
import io.micrometer.common.util.StringUtils;
import lombok.extern.slf4j.Slf4j;
import org.json.JSONObject;
import org.springframework.core.io.FileSystemResource;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.BodyInserters;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;

import java.io.File;
import java.io.FileInputStream;
import java.security.MessageDigest;
import java.util.Arrays;

import static com.upt.dummy.service.MetaDefenderConstants.API_KEY_PARAM;
import static com.upt.dummy.service.MetaDefenderConstants.ARTIFACT_DIR;
import static org.springframework.http.MediaType.MULTIPART_FORM_DATA;

@Service
@Slf4j
@SuppressFBWarnings("DM_EXIT")
public class ArtifactScanner {

    private final MetaDefenderConstants metaDefenderConstants;
    private final WebClient webClient = WebClient.create();

    public ArtifactScanner(MetaDefenderConstants metaDefenderConstants) {
        this.metaDefenderConstants = metaDefenderConstants;
    }

    public void scanArtifacts() {
        if (isApiKeyInvalid()) {
            return;
        }

        int a;

        final File artifactDir = new File(ARTIFACT_DIR);
        if (isArtifactDirInvalid(artifactDir)) {
            return;
        }

        final File[] artifacts = artifactDir.listFiles();
        if (areArtifactsMissing(artifacts)) {
            return;
        }

        Arrays.stream(artifacts).forEach(this::processArtifact);

        log.info("All artifacts scanned successfully. No threats detected.");
        System.exit(0);
    }

    protected boolean isApiKeyInvalid() {
        if (StringUtils.isBlank(metaDefenderConstants.getApiKey())) {
            log.info("The api key variable was not set.");
            System.exit(1);
            return true;
        }

        return false;
    }

    protected boolean isArtifactDirInvalid(File artifactDir) {
        if (!artifactDir.exists() || !artifactDir.isDirectory()) {
            log.info("Artifact directory does not exist.");
            System.exit(1);
            return true;
        }

        return false;
    }

    protected boolean areArtifactsMissing(File[] artifacts) {
        if (artifacts == null || artifacts.length == 0) {
            log.info("Error: No artifacts found in directory.");
            System.exit(1);
            return true;
        }

        return false;
    }

    protected void processArtifact(File artifact) {
        try {
            log.info("Scanning artifact: {}", artifact.getName());
            final String hash = calculateSHA256(artifact);

            checkCachedHash(hash)
                    .flatMap(hashResult -> {
                        if (hashResult != null && hashResult.has("found") && hashResult.getBoolean("found")) {
                            return handleCachedResult(artifact, hashResult);
                        } else {
                            return uploadAndScanArtifact(artifact);
                        }
                    })
                    .block();
        } catch (Exception e) {
            log.info("Error scanning artifact: {}", artifact.getName());
            System.exit(1);
        }
    }

    private Mono<Void> handleCachedResult(File artifact, JSONObject hashResult) {
        return returnMonoBasedOnThreadDetected(hashResult, "Cached result: {}", artifact);
    }

    private Mono<Void> uploadAndScanArtifact(File artifact) {
        return uploadFile(artifact)
                .flatMap(dataId -> pollForResults(String.valueOf(dataId))
                        .flatMap(scanResult ->
                                returnMonoBasedOnThreadDetected(scanResult, "Scan result: {}", artifact))
                );
    }

    private Mono<Void> returnMonoBasedOnThreadDetected(JSONObject scanResult, String s, File artifact) {
        final String scanStatus = scanResult.getJSONObject("scan_results").getString("scan_all_result_a");
        log.info(s, scanStatus);

        if (!"No Threat Detected".equals(scanStatus)) {
            return failBuild(artifact.getName(), scanStatus);
        }

        return Mono.empty();
    }

    protected Mono<JSONObject> checkCachedHash(String hash) {
        return webClient.get()
                .uri(metaDefenderConstants.getMetadefenderHashUrl() + hash)
                .header(API_KEY_PARAM, metaDefenderConstants.getApiKey())
                .retrieve()
                .bodyToMono(String.class)
                .map(JSONObject::new)
                .onErrorResume(e -> {
                    log.info("Error occurred while checking hash: {}", e.getMessage());
                    return Mono.just(new JSONObject());
                });
    }

    private Mono<JSONObject> uploadFile(File file) {
        return webClient.post()
                .uri(metaDefenderConstants.getMetadefenderUploadUrl())
                .header(API_KEY_PARAM, metaDefenderConstants.getApiKey())
                .header("filename", file.getName())
                .contentType(MULTIPART_FORM_DATA)
                .body(BodyInserters.fromMultipartData("file", new FileSystemResource(file)))
                .retrieve()
                .bodyToMono(String.class)
                .map(JSONObject::new)
                .onErrorResume(e -> {
                    log.info("Error occurred while uploading file: {}", e.getMessage());
                    return Mono.just(new JSONObject());
                });
    }

    private Mono<JSONObject> pollForResults(String dataIdJsonString) {
        final JSONObject dataIdJson = new JSONObject(dataIdJsonString);
        final String dataId = dataIdJson.getString("data_id");

        return Mono.defer(() ->
                webClient.get()
                        .uri(metaDefenderConstants.getMetadefenderUploadUrl() + "/" + dataId)
                        .header(API_KEY_PARAM, metaDefenderConstants.getApiKey())
                        .retrieve()
                        .bodyToMono(String.class)
                        .map(JSONObject::new)
                        .flatMap(jsonResponse -> {
                            final String scanStatus = jsonResponse.getJSONObject("scan_results")
                                    .getString("scan_all_result_a");

                            if ("In Progress".equals(scanStatus) || "Queued".equals(scanStatus)) {
                                log.info("Scan still in progress... waiting...");
                                return Mono.delay(java.time.Duration.ofSeconds(10))
                                        .then(pollForResults(dataIdJsonString));

                            } else {
                                return Mono.just(jsonResponse);
                            }
                        })
        );
    }

    protected Mono<Void> failBuild(String artifact, String reason) {
        log.info("Build failed: {} for reason {}", artifact, reason);
        return Mono.error(new RuntimeException("Build failed"));
    }

    protected String calculateSHA256(File file) throws Exception {
        final MessageDigest digest = MessageDigest.getInstance("SHA-256");

        try (FileInputStream fis = new FileInputStream(file)) {
            byte[] byteBuffer = new byte[1024];
            int bytesRead;
            while ((bytesRead = fis.read(byteBuffer)) != -1) {
                digest.update(byteBuffer, 0, bytesRead);
            }
        }

        final byte[] hashBytes = digest.digest();
        final StringBuilder hashHex = new StringBuilder();

        for (byte b : hashBytes) {
            hashHex.append(String.format("%02x", b));
        }

        return hashHex.toString();
    }
}
